# SMS - Student ManagementSystem
A system to manage records of students and subjects they are enrolled in.
## Instructions to Setup

 1. Clone the repo to local/development environment.
 2. Change the THEME_URL in autoloader.php to the domain which will be used to access the system.
 3. Restore setup/student_management.sql in to a mysql database.
 4. Change the database credentials in config/local.xml
 5. Load the site using the domain. Form can be seen on BASE_URL/student/index
## Technology used in development environment
 
 1. Ubuntu 16.04
 2. PHP 7.0
 3. MySQL 5.7.22
 4. jQuery 3.3.1
 5. PDO for Database queries
 6. HTML / CSS
 
## Routing
 
 URL endpoints will be created automatically when a controller is created and function is implemented for action. URL structure is built up like bellow.
 
 ROOT_DOMAIN/CONTROLLER_NAME/ACTION
 
 CONTROLLER_NAME is the controller class.
 ACTION is the function implemented in controller.
 
## Views
 
 Custom views can be loaded for controllers by calling to the render($name) function. And array of data can be passed to the view in the same function. And those data can be retrieved from view page as $data.
 Ex: $this->view->render('studentPage', $data);
