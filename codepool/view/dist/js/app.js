/**
 * validation required fields
 * @returns {number}
 */
var validation = function () {
    var n_flag = 0;
    $(".required").each(function () {
        if ($(this).val() == '') {
            n_flag = 1;
            return false;
        }
    });
    return n_flag;
};

/**
 * validation required fields of sales rep create form submit
 */
$("#student-form").submit(function (e) {
    var flag = 0;
    flag = validation();

    $(".required").each(function () {
        if ($(this).val() == '') {
            $(this).css('border-color', 'red');
            $(".error-msg-box").css("display", "block");
            $("#show-msg").html("Please fill required fields before submit");
            e.preventDefault();
        }
    });

    hideNotification();
});

/** to hide the notification message in few seconds */
var hideNotification = function () {
    setTimeout(function () {
        $(".notification-hide").fadeOut("slow");
        $(".required").each(function () {
            $(this).css('border-color', '#ccc');
        });
    }, 7000);

}

$( document ).ready(function() {
    hideNotification();
});
