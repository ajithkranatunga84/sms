<?php get_head(); ?>
<?php
$subjects = $data['subjects'];
$records = $data['records'];
?>
<div class="sms-wrapper">
    <div class="form-header">Student Management System</div>
    <div class="form-content">
        <form action="<?= get_site_url(); ?>/student/index/" method="post" id="student-form">
            <input type="hidden" name="student-form" value="Y">
            <?php if($data['message'] && ($data['message']!="")) : ?>
            <div class="msg-box notification-hide">
                <div id="success-msg"><?= $data['message']; ?></div>
            </div>
            <?php endif; ?>
            <div class="error-msg-box notification-hide">
                <div id="show-msg"></div>
            </div>
            <div class="element-wrapper">
                <label for="firstName">First Name</label>
                <input type="text" class="required" name="firstName">
            </div>
            <div class="element-wrapper">
                <label for="lastName">Last Name</label>
                <input type="text" class="required" name="lastName">
            </div>
            <div class="element-wrapper">
                <label for="subjects">Subjects</label>
                <select multiple="multiple" class="required subject-select" name="subjects[]">
                    <?php foreach ($subjects as $subject) : ?>
                        <option value="<?=$subject->getSubjectId(); ?>"><?= $subject->getSubjectName(); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="submit-wrapper">
                <input type="submit" value="Save">
            </div>
        </form>
    </div>

    <div class="result-table">
        <h2 class="table-caption">Subjects List</h2>
        <table>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Subjects</th>
            </tr>
            <?php foreach ($records as $record) : ?>
            <tr>
                <td><?= $record->getFirstName(); ?></td>
                <td><?= $record->getLastName(); ?></td>
                <td><?= $record->getSubjectNames(); ?></td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
    <div class="form-footer">&copy; S M S - 2018</div>
</div>
<?php get_foot(); ?>