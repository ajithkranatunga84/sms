<?php

/**
 * @author: Ajith K Ranatunga
 *
 * Class Student
 */
class Student extends BaseController
{
    /**
     * @var Student
     */
    protected $studentModel;

    /**
     * @var SubjectModel
     */
    protected $subjectModel;

    /**
     * Student constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->subjectModel = new SubjectModel();
        $this->studentModel = new StudentModel();
    }

    /**
     * @param $student
     * @return mixed
     */
    public function create($student)
    {
        $stId = $this->studentModel->create($student);
        return $stId;
    }

    /**
     * Handling form submission and loading page with data
     */
    public function index()
    {
        $data = array();
        /**
         * If form is submitted.
         */
        if(isset($_POST['student-form']) && ($_POST['student-form'] == 'Y')){
            $student = new StudentDao();
            $student->setFirstName(htmlspecialchars($_POST['firstName']));
            $student->setLastName(htmlspecialchars($_POST['lastName']));
            $student_id = $this->create($student);
            $student->setStudentId($student_id);

            $subjects = $_POST['subjects'];
            $stSubjects = array();
            foreach ($subjects as $subject){
                $stSubjects[] = array($student->getStudentId(), $subject);
            }
            $stSubjectModel = new StudentSubjectModel();
            $stSubjectModel->create($stSubjects);
            $data['message'] = "Student added successfully";
        }

        /**
         * loading data for subject dropdown and student/subject list
         */
        $data['subjects'] = $this->subjectModel->getDistinctSubjectNames();
        $data['records'] = $this->getAllRecords();

        $this->view->render('studentPage', $data);
    }

    /**
     * @return mixed
     */
    public function getAllRecords()
    {
        return $this->studentModel->getAllRecords();
    }
}