<?php
/**
 * @author: Ajith K Ranatunga
 * Loading all the required files
 */

if (!defined('ABSPATH'))
    define('ABSPATH', dirname(__FILE__) . '/');

if (!defined('SITE_URL')) {
    define("SITE_URL", 'http://dev.sms.com/');
}

if(!define("THEME_URL"))
    define("THEME_URL", SITE_URL . 'view/');

foreach (glob("libs/*.php") as $filename) {
    require_once($filename);
}
foreach (glob("Controller/*.php") as $filename) {
    require_once($filename);
}
foreach (glob("Dao/*.php") as $filename) {
    require_once($filename);
}
foreach (glob("Model/*.php") as $filename) {
    require_once($filename);
}