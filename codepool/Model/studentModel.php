<?php
/**
 * @author: Ajith K Ranatunga
 *
 * Class StudentModel
 */

class StudentModel extends BaseModel
{

    /**
     * Student constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $student
     * @return string
     */
    public function create($student)
    {
        $createStudentQuery = "INSERT INTO student(first_name, last_name) VALUES (:firstName, :lastName)";

        try{
            $conn = $this->database->load();
            $stmt = $conn->prepare($createStudentQuery);

            $stmt->bindParam(':firstName', $firstName, PDO::PARAM_STR, 25);
            $stmt->bindParam(':lastName', $lastName, PDO::PARAM_STR, 25);

            $firstName = $student->getFirstName();
            $lastName = $student->getLastName();

            $stmt->execute();
            $studentId = $conn->lastInsertId();

            return $studentId;

        } catch (PDOException $e) {
            echo "ERROR : " . $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function getAllStudents()
    {
        $students = $this->loadAll('student', StudentDao::class);
        return $students;
    }

    /**
     * @return array
     */
    public function getAllRecords()
    {
        $query = "SELECT ss.student_id, s.first_name, s.last_name, group_concat(su.subject_name) subject_names
                    FROM student_subject as ss
                      JOIN student as s ON ss.student_id = s.student_id
                      JOIN subject as su ON ss.subject_id= su.subject_id
                    GROUP BY ss.student_id";
        try{
            $conn = $this->database->load();
            $stmt = $conn->prepare($query);

            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS, StudentSubjectDao::class);

            $result = $stmt->fetchAll();

            return $result;

        } catch (PDOException $e) {
            echo "ERROR : " . $e->getMessage();
        }
    }
}