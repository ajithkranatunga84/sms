<?php
/**
 * @author: Ajith K Ranatunga
 *
 * Class SubjectModel
 */

class SubjectModel extends BaseModel
{

    /**
     * SubjectModel constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $subject
     * @return string
     */
    public function create($subject)
    {
        $createSubjectQuery = "INSERT INTO subject(subject_name) VALUES (:subjectName)";

        try{
            $conn = $this->database->load();
            $stmt = $conn->prepare($createSubjectQuery);

            $stmt->bindParam(':subjectName', $subjectName, PDO::PARAM_STR, 25);

            $firstName = $subject->getSubjectName();

            $stmt->execute();
            $subjectId = $conn->lastInsertId();

            return $subjectId;

        } catch (PDOException $e) {
            echo "ERROR : " . $e->getMessage();
        }
    }

    /**
     * @return array
     */
    public function getDistinctSubjectNames()
    {
        $query = "SELECT DISTINCT subject_name, subject_id FROM subject";
        try {
            $conn = $this->database->load();
            $stmt = $conn->prepare($query);

            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS, SubjectDao::class);

            $result = $stmt->fetchAll();

            return $result;
        } catch (PDOException $e) {
            echo "ERROR : " . $e->getMessage();
        }
    }
}