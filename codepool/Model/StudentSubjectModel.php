<?php

class StudentSubjectModel extends BaseModel
{
    public function __construct()
    {
        parent::__construct();
    }

    public function create($studentSubjects)
    {
        $qmarks = '(?, ?)'. str_repeat(',(?, ?)', count($studentSubjects)-1);
        $query = "INSERT INTO student_subject(student_id, subject_id) VALUES ".$qmarks;
        $values_array = array();

        foreach ($studentSubjects as $stsubs){
            $values_array = array_merge($values_array, $stsubs);
        }

        try{
            $conn = $this->database->load();
            $stmt = $conn->prepare($query);
            $stmt->execute($values_array);
        } catch (PDOException $e) {
            echo "ERROR : " . $e->getMessage();
        }
    }
}