<?php
/**
 * @author: Ajith K Ranatunga
 *
 * Class BaseController
 */

class BaseController
{
    public $view;

    function __construct()
    {
        $this->view = new View();
    }

    function addNew($name = '')
    {
        $this->view->addNew($name);
    }
}