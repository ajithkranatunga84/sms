<?php

function get_head()
{
    require_once(ABSPATH . 'view/head.php');
}

function get_foot()
{
    require_once(ABSPATH . 'view/foot.php');
}

function get_theme_dir()
{
    return THEME_URL;
}

function get_site_url(){
    return SITE_URL;
}