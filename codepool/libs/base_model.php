<?php
/**
 * @author: Ajith K Ranatunga
 *
 * Class BaseModel
 */

class BaseModel
{
    /**
     * @var DbConnect
     */
    protected $database;

    /**
     * BaseModel constructor.
     */
    function __construct()
    {
        $this->database = new DbConnect();
    }

    /**
     * @param $id
     * @param $objecType
     * @return mixed
     */
    public function getById($id, $objecType)
    {
        $object = new $objecType;
        //TODO: implement getById
        return $object;
    }

    /**
     * @param $table_name
     * @param $object_name
     * @param string $extra
     * @return mixed
     */
    function loadAll($table_name, $objectType, $extra = '')
    {
        $query = "SELECT * FROM " . $table_name . $extra;
        try {
            $conn = $this->database->load();
            $stmt = $conn->prepare($query);

            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS, $objectType);

            $result = $stmt->fetchAll();

            return $result;
        } catch (PDOException $e) {
            echo "ERROR : " . $e->getMessage();
        }
    }
}
