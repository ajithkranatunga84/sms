<?php
/**
 * @author: Ajith K Ranatunga
 *
 * Class DbConnect
 */

class DbConnect
{

    /**
     * DbConnect constructor.
     */
    public function __construct()
    {
        $con = $this->load();
    }

    /**
     * @return PDO
     */
    public function load()
    {
        $dbVars = $this->getDbCredentials();
        $dbType = $dbVars->database->dbType;
        $dbHost = $dbVars->database->dbHost;
        $dbUser = $dbVars->database->dbUser;
        $dbPassword = $dbVars->database->dbPassword;
        $dbName = $dbVars->database->dbName;
        try {
            $conn = new PDO($dbType . ':host=' . $dbHost . ';dbname=' . $dbName,
                $dbUser , $dbPassword);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $conn;
        } catch (PDOException $e) {
            echo "Error : " . $e->getMessage();
        }

    }

    /**
     * @return SimpleXMLElement
     */
    public function getDbCredentials()
    {
        try {
            return simplexml_load_file(ABSPATH.'/config/local.xml');
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}