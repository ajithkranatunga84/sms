<?php
/**
 * @author: Ajith K Ranatunga
 *
 * Class View
 */

class View
{
    /**
     * View constructor
     */
    function __construct()
    {
//        $data['username'] = "Ajith";
    }

    /**
     * @param $name / view name
     * @param null $data
     * @return null
     * this loads a view file and returned data are accessible using $data parameter
     */
    public function render($name, $data = null)
    {
        $view_file = ABSPATH . 'view/pages/' . $name . ".php";
        if (file_exists($view_file)) {
            require_once($view_file);
        } else {
            require_once(ABSPATH . 'view/pages/error.php');
        }
        return $data;
    }
}