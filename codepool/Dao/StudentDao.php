<?php
/**
 * @author: Ajith K Ranatunga
 *
 * Class StudentDao
 */

class StudentDao
{
    /**
     * @var $student_id
     */
    private $student_id;
    /**
     * @var $first_name
     */
    private $first_name;
    /**
     * @var $last_name
     */
    private $last_name;

    /**
     * @return mixed
     */
    public function getStudentId()
    {
        return $this->student_id;
    }

    /**
     * @param mixed $student_id
     */
    public function setStudentId($student_id)
    {
        $this->student_id = $student_id;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param mixed $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

}