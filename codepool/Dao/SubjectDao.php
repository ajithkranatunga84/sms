<?php
/**
 * Created by PhpStorm.
 * User: ajith
 * Date: 7/17/18
 * Time: 9:01 PM
 */

class SubjectDao
{
    protected $subject_id;
    protected $subject_name;

    /**
     * @return mixed
     */
    public function getSubjectId()
    {
        return $this->subject_id;
    }

    /**
     * @param mixed $subject_id
     */
    public function setSubjectId($subject_id)
    {
        $this->subject_id = $subject_id;
    }

    /**
     * @return mixed
     */
    public function getSubjectName()
    {
        return $this->subject_name;
    }

    /**
     * @param mixed $subject_name
     */
    public function setSubjectName($subject_name)
    {
        $this->subject_name = $subject_name;
    }

}