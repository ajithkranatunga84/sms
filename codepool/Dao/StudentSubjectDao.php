<?php
class StudentSubjectDao
{
    protected $student_id;
    protected $first_name;
    protected $last_name;
    protected $subject_names;

    /**
     * @return mixed
     */
    public function getStudentId()
    {
        return $this->student_id;
    }

    /**
     * @param mixed $student_id
     */
    public function setStudentId($student_id)
    {
        $this->student_id = $student_id;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param mixed $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * @return mixed
     */
    public function getSubjectNames()
    {
        return $this->subject_names;
    }

    /**
     * @param mixed $subject_names
     */
    public function setSubjectNames($subject_names)
    {
        $this->subject_names = $subject_names;
    }

}